document.getElementById('signupForm').addEventListener('submit', function(event) {
  event.preventDefault();
  // Récupérer les valeurs du formulaire
  var nom = document.getElementById('nom').value;
  var prenom = document.getElementById('prenom').value;
  var email = document.getElementById('email').value;
  var password = document.getElementById('password').value;
  // Vous pouvez également gérer le fichier de la carte d'identité si nécessaire

  // Rediriger vers la page de bienvenue du client avec le nom et le prénom
  window.location.href = 'welcome_utilisateur.html';
});
