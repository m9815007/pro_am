document.getElementById('loginBtn').addEventListener('click', function() {
    // Rediriger vers la page de connexion
    window.location.href = 'login.html';
  });
  
  document.getElementById('signupBtn').addEventListener('click', function() {
    // Afficher la sélection du type de compte
    document.getElementById('userTypeSelection').style.display = 'block';
  });
  
  document.getElementById('userBtn').addEventListener('click', function() {
    // Rediriger vers la page d'inscription en tant qu'utilisateur
    window.location.href = 'signup_utilisateur.html';
  });
  
  document.getElementById('clientBtn').addEventListener('click', function() {
    // Rediriger vers la page d'inscription en tant que client
    window.location.href = 'signup_client.html';
  });
  